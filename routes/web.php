<?php
Route::get('/home', 'HomeController@index')->name('home_page');
Route::get('/show/{name?}', 'HomeController@show')->name('show_page');
Route::get('/update/{id}', 'HomeController@update');
