<?php
namespace App\Http\Controllers;

class HomeController extends Controller {

    public function index()
    {
        return view('welcome');
    }

    public function show($name = null)
    {
        if ($name === null) {
            return redirect(route('home_page'));
        }

        $test = '<p>test data</p>';

//        return view('show', compact('name', 'test'));
        return view('show', [
            'name' => $name,
            'test' => $test
        ]);
//        return view('show')->with('username', $name)->with('test', $test);
    }

    public function update($id)
    {
        if ($id === 5) {
            return view('welcome');
        }

        return redirect(route('show_page', $id));
    }
}
